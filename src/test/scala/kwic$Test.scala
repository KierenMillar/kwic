import org.scalatest.FunSuite


/**
  * Created by kierenmillar on 22/03/2016.
  */
class kwic$Test extends FunSuite{

  test("test getkeywords"){
    assert(kwic.getKeyWords("abc' 123 ago TOM") === Array("abc'","tom"))
  }

  test("test createMap"){
    assert(kwic.createMap(Array("abc", "bcd", "cde", "def")) === Map("abc" -> List(0), "bcd" -> List(1), "cde" -> List(2), "def" -> List(3)))
  }

}
