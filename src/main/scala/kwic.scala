import java.io.{PrintWriter, File}
import java.nio.file.{Files, Paths}

import scala.collection.immutable.TreeMap
import scala.collection.mutable.ArrayBuffer


/**
  * Created by kierenmillar on 22/03/2016.
  */
object kwic {

  val stopWords = getStopWords
  val filesMap = collection.mutable.Map[String,Array[String]]()

  def getStopWords : Array[String] = {
    scala.io.Source.fromFile("stop_words.txt").getLines().toArray
  }

  def main(args: Array[String]): Unit = {
    println("Please enter your filename/s, empty to finish input")
    requestFileNames()
    val KeywordIndexes = TreeMap(filesMap.mapValues(a => createMap(a)).toSeq:_*)
    writeToFile(stringsToWrite(KeywordIndexes))
  }

  def requestFileNames() : Unit = {
    val fileName = scala.io.StdIn.readLine()
    val boolean = Files.exists(Paths.get(fileName))
    fileName match{
      case ("") => println("index being created...")
      case (_) => boolean match {
        case true => filesMap(fileName) = scala.io.Source.fromFile(fileName).getLines().toArray
          requestFileNames()
        case false => println("invalid filename")
          requestFileNames()
      }
    }
  }

  def createMap (anArray: Array[String]) : TreeMap[String,List[Int]] ={
    val map = collection.mutable.Map[String,List[Int]]()
    for( i <- anArray.indices){
      getKeyWords(anArray(i)).foreach(a => if (map contains a) map(a) = map(a) :+ i else map(a) = List(i))
    }
    TreeMap(map.toSeq:_*)
  }

  def getKeyWords(str : String) : Array[String] = {
    var temp = str.toLowerCase.replaceAll("[^a-z ']","").replaceAll("\\s+"," ").split(" ")
    //temp.distinct.diff(stopWords)
    for (str <- stopWords) {
      temp = temp.filterNot(_.equals(str))
    }
    temp
  }

  def writeToFile (arr : Array[String]) = {
    val file = new File("kwic-index.txt")
    val writer = new PrintWriter(file)
    arr.foreach(str => writer.write(str + "\n"))
    /**for (str <- arr){
      writer.write(str + "\n")
    }*/
    writer.close()
  }

  def stringsToWrite (map : TreeMap[String, TreeMap[String, List[Int]]]) : Array[String] ={
    val rtn = ArrayBuffer.empty[String]
    for ((filename, keywordmap) <- map){
      rtn += ("\n" + filename + "\n")
      for((keyword,indexes) <- keywordmap){
        var previousIndex = 0
        var previousKeyIndex = 0
        for(index <- indexes){
          if (previousIndex != index) previousKeyIndex = 0
          val str = filesMap(filename)(index)
          val indexOfKeyword = str.toLowerCase.indexOf(keyword, previousKeyIndex)
          val wordsbefore = str.slice(indexOfKeyword - 30,indexOfKeyword)
          val wordsafter = str.slice(indexOfKeyword + keyword.length, indexOfKeyword + keyword.length + 30)
          previousKeyIndex = indexOfKeyword + 1
          previousIndex = index
          rtn += (index+1 + " " + f"$wordsbefore%30s" + " " + keyword + f"$wordsafter%-30s")
        }
      }
    }
    rtn.toArray
  }


}
